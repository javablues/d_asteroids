# d-asteroids
## Game in D programming language with raylib
#### Work in progress...

![s1](screenshot/screenshot-1.png)

## License

1. You can use all code under the MIT License
2. You can use resource files (graphics, music, sound effects) as PRIVATE USE only (just on your personal computer without any distribution)

## Installation

### Install on Windows

1. Install Dlang for Windows from this page: https://dlang.org/download.html
2. Remember to set path to 64 bit Dlang version in Environment Variables section
```bash
C:\D\dmd2\windows\bin64
```

3. Put to the project home folder two files:
```bash
raylib.dll
raylibdll.lib
```

downloaded from here (**raylib-4.x.x_win64_msvc16.zip**): https://github.com/raysan5/raylib/releases/

### Install on Linux (arch example)

1. Install Dlang
```bash
yay dlang
```
2. Install raylib
```bash
yay raylib
```

## Run

1. Pull project to your projects folder.
2. Run `dub`
```bash
cd ~/projects/d_asteroids
dub
```
