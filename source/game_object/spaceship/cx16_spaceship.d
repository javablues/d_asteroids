module cx16_spaceship;

import raylib;
import game_object;
import shield;
import motion;

class CX16Spaceship : GameObject {
  import game_const;
  import object_name;

  Shield shield;
  const float scaledObjectWidth;
  const float scaledObjectHeight;

  this() {
    objectName=ObjectName.spaceship;
    objTexture=LoadTexture("resources/spaceship/CX16-X1.png");
    SetTextureFilter(objTexture, 1);

    scaledObjectWidth=objTexture.width*GameConst.scale;
    scaledObjectHeight=objTexture.height*GameConst.scale;

    src=Rectangle(0, 0, objTexture.width, objTexture.height);
    dest=Rectangle(0, 0, scaledObjectWidth, scaledObjectHeight);
    origin=Vector2(scaledObjectWidth/2, scaledObjectHeight/2);
    motion=Motion(200, 200, 0, 0, 4f);

    shield=new Shield(&this.motion.dx, &this.motion.dy, &this.motion.rotation);
    shield.active=false;

    rectCollision=Rectangle(0, 0, scaledObjectWidth, scaledObjectHeight);
    isColliding=false;
  }

  override bool isActive() {
    return this.active;
  }

  override void processInput() {
    import game_scene;
    import bullet;

    // fire bullet
    if(IsKeyPressed(KeyboardKey.KEY_SPACE)) {
      GameScene.gameObjects~=new Bullet(this.dest.x, this.dest.y, this.motion.rotation);
    }

    // run shield
    if(IsKeyPressed(KeyboardKey.KEY_S)) {
      shield=new Shield(&this.motion.dx, &this.motion.dy, &this.motion.rotation);
      GameScene.gameObjects~=shield;
    }
    if(IsKeyReleased(KeyboardKey.KEY_S)) {
      shield.active=false;
    }

    // spaceship movement
    if(IsKeyDown(KeyboardKey.KEY_D)) this.motion.rotation+=4f;
    if(IsKeyDown(KeyboardKey.KEY_A)) this.motion.rotation-=4f;
    if(IsKeyDown(KeyboardKey.KEY_W)) {
      this.motion.acceleration=8f;
    }
    else {
      if (this.motion.acceleration>0.4f) this.motion.acceleration-=0.04f;
    }
  }

  override void update() {
    import std.math: cos, sin;
    import game_scene;
    import explosion;
    import gfx_helper;

    this.motion.dx+=cos(this.motion.rotation*GameConst.radian)*this.motion.acceleration;
    this.motion.dy+=sin(this.motion.rotation*GameConst.radian)*this.motion.acceleration;
    this.dest.x=this.motion.dx;
    this.dest.y=this.motion.dy;
    this.rectCollision.x=this.dest.x-scaledObjectWidth/2;
    this.rectCollision.y=this.dest.y-scaledObjectHeight/2;

    if(this.isColliding) {
      if(!this.shield.active) {
        GameScene.gameObjects~=new Explosion(this.dest.x, this.dest.y, this.motion.rotation, Fade(Colors.BLACK, 0.05f));
      }
      this.motion.rotation=this.motion.rotation-10;
      this.motion.acceleration=8f;
      this.isColliding=false;
    }

    GfxHelper gfxHelper;
    if(gfxHelper.isOffScreen(this.dest.x, this.dest.y)) {
      const auto oppositeEdge=gfxHelper.getPositionOnOppositeEdge(this.dest.x, this.dest.y);
      this.motion.dx=oppositeEdge.x;
      this.motion.dy=oppositeEdge.y;
    }
  }

  override void render() {
    DrawTexturePro(objTexture, this.src, this.dest, this.origin, this.motion.rotation+90, Colors.WHITE);
    debug(rects) {
      DrawRectangleLinesEx(this.dest, 2, Colors.RED);
      DrawRectangleLinesEx(this.rectCollision, 2, Colors.BLUE);
    }
  }

} // end of class
