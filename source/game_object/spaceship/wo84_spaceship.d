module wo84_spaceship;

import std.math;
import raylib;
import game_object;
import game_const;
import motion;

class WO84Spaceship {
  immutable float scale=0.25f;
  immutable Texture2D spaceshipTexture;
  immutable Rectangle src={0, 0, 140, 160};

  Rectangle dest={200, 200, 140*scale, 160*scale};
  Vector2 origin={140*scale/2, 160*scale/2};
  Motion motion={dx:160, dy:140, rotation:0, maxSpeed:0, acceleration:0};

  this() {
    this.spaceshipTexture=LoadTexture("resources/spaceship/WO84-wu-X1.png");
  }

  ~this() {
    UnloadTexture(this.spaceshipTexture);
  }

  bool isActive() {
    return true;
  }

  void processInput() {
    if(IsKeyDown(KeyboardKey.KEY_RIGHT)) this.motion.rotation += 4f;
    if(IsKeyDown(KeyboardKey.KEY_LEFT)) this.motion.rotation -= 4f;
  }

  void update() {
    this.motion.dx += cos(this.motion.rotation*GameConst.radian)*this.motion.acceleration;
    this.motion.dy += sin(this.motion.rotation*GameConst.radian)*this.motion.acceleration;
    this.dest.x=this.motion.dx;
    this.dest.y=this.motion.dy;
  }

  void render() {
    DrawTextureTiled(spaceshipTexture, this.src, this.dest, this.origin, this.motion.rotation+90, this.scale, Colors.WHITE);
  }

} // end of class
