module bullet;

import raylib;
import game_object;
import motion;

class Bullet : GameObject {
  import game_const;
  import object_name;

  this(float x, float y, float rotation) {
    objectName=ObjectName.bullet;
    objTexture=LoadTexture("resources/weapon/bullet_short_single.png");
    src=Rectangle(0, 0, objTexture.width, objTexture.height);
    dest=Rectangle(0, 0, objTexture.width*GameConst.scale, objTexture.height*GameConst.scale);
    origin=Vector2(objTexture.width*GameConst.scale/2, objTexture.height*GameConst.scale/2);
    rectCollision=Rectangle(0, 0, objTexture.width, objTexture.height);
    motion=Motion(0, 0, 0, 0, 48f);
    dest.x=x;
    dest.y=y;
    motion.dx=x;
    motion.dy=y;
    motion.rotation=rotation;
  }

  override bool isActive() {
    return this.active;
  }

  override void processInput() {
    // bullet is fired by spaceship
  }

  override void update() {
    import std.math;
    import gfx_helper;
    import game_scene;
    import explosion;

    this.motion.dx+=cos(this.motion.rotation*GameConst.radian)*this.motion.acceleration;
    this.motion.dy+=sin(this.motion.rotation*GameConst.radian)*this.motion.acceleration;
    this.dest.x=this.motion.dx;
    this.dest.y=this.motion.dy;

    this.rectCollision.x=this.dest.x-objTexture.width/2;
    this.rectCollision.y=this.dest.y-objTexture.height/2;

    // quick shot but slow flight of bullet
    if(this.motion.acceleration>24f) {
      this.motion.acceleration-=2f;
    }

    if(this.isColliding) {
      GameScene.gameObjects~=new Explosion(this.dest.x, this.dest.y, this.motion.rotation);
      this.active=false; // bullet reached target, it can be removed from scene now
    }

    GfxHelper gfxHelper;
    if(this.active && gfxHelper.isOffScreen(this.dest.x, this.dest.y)) {
      this.active=false;
    }
  }

  override void render() {
    DrawTexturePro(objTexture, this.src, this.dest, this.origin, this.motion.rotation+90, Colors.WHITE);
    debug(rects) {
      DrawRectangleLinesEx(this.rectCollision, 2, Colors.BLUE);
    }
  }

} // end of class
