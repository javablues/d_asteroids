module shield;

import raylib;
import game_object;

class Shield : GameObject {
  import game_const;
  import object_name;

  const Rectangle[6] frames=[
    Rectangle(  0,   0, 280, 280),
    Rectangle(280,   0, 280, 280),
    Rectangle(560,   0, 280, 280),
    Rectangle(  0, 280, 280, 280),
    Rectangle(280, 280, 280, 280),
    Rectangle(560, 280, 280, 280),
  ];

  float scaledObjectWidth;
  float scaledObjectHeight;

  // below params taken from spaceship
  float* x;
  float* y;
  float* rotation;

  this(float* x, float* y, float* rotation) {
    objectName=ObjectName.shield;
    objTexture=LoadTexture("resources/weapon/shield_frames.png");
    SetTextureFilter(objTexture, 1);

    scaledObjectWidth=280*GameConst.scale;
    scaledObjectHeight=280*GameConst.scale;

    dest=Rectangle(0, 0, scaledObjectWidth, scaledObjectHeight);
    origin=Vector2(scaledObjectWidth/2, scaledObjectHeight/2);
    rectCollision=Rectangle(0, 0, scaledObjectWidth, scaledObjectHeight);

    this.x=x;
    this.y=y;
    this.rotation=rotation;
  }

  override bool isActive() {
    return this.active;
  }

  float time=0f;
  int frame=0;
  int shieldRotation=0;
  int rotFactor=-40;

  override void update() {
    time+=GetFrameTime();
    if(time>=0.01f) {
      this.frame++;
      this.time=0f;
    }
    this.frame=this.frame % 6;

    this.shieldRotation=this.shieldRotation+this.rotFactor;
    if(this.shieldRotation>=360) this.rotFactor=-12;
    if(this.shieldRotation<=0) this.rotFactor=+12;

    this.rectCollision.x=*this.x-scaledObjectWidth/2;
    this.rectCollision.y=*this.y-scaledObjectHeight/2;
  }

  override void render() {
    dest.x=*this.x;
    dest.y=*this.y;
    DrawTexturePro(objTexture, frames[this.frame], dest, this.origin, this.shieldRotation + *this.rotation, Fade(Colors.RED, 0.6f));
    debug(rects) {
      DrawRectangleLinesEx(dest, 2, Colors.RED);
      DrawRectangleLinesEx(this.rectCollision, 2, Colors.BLUE);
    }
  }

} // end of class
