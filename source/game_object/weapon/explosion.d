module explosion;

import raylib;
import game_object;

class Explosion : GameObject {
  import game_const;

  Color color;
  const Rectangle[9] frames=[
    Rectangle(   0, 0,  140, 140),
    Rectangle( 140, 0,  140, 140),
    Rectangle( 280, 0,  140, 140),
    Rectangle( 420, 0,  140, 140),
    Rectangle( 560, 0,  140, 140),
    Rectangle( 700, 0,  140, 140),
    Rectangle( 840, 0,  140, 140),
    Rectangle( 980, 0,  140, 140),
    Rectangle(1120, 0,  140, 140)
  ];

  this(float x, float y, float rotation) {
    this(x, y, rotation, Colors.WHITE);
  }

  this(float x, float y, float rotation, Color color) {
    objTexture=LoadTexture("resources/explosion/explosion.png");
    SetTextureFilter(objTexture, 1);

    origin=Vector2(70/2, 70/2);
    this.motion.dx=x;
    this.motion.dy=y;
    this.motion.rotation=rotation;
    this.color=color;
  }

  override bool isActive() {
    return this.active;
  }

  float time=0f;
  int frame=0;

  override void update() {
    time+=GetFrameTime();
    if(time>=0.04f) {
      this.frame++;
      this.time=0f;
    }
    if(this.frame==8) { // idx of last frame
      this.active=false;
    }
  }

  override void render() {
    auto dest=Rectangle(this.motion.dx, this.motion.dy, frames[this.frame].w*GameConst.scale*2, frames[this.frame].h*GameConst.scale*2);
    DrawTexturePro(objTexture, frames[this.frame], dest, this.origin, this.motion.rotation, this.color);
  }
}
