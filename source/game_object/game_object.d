module game_object;

import renderable;
import updatable;
import inputable;
import activable;
import motion;
import object_name;

class GameObject : Renderable, Updatable, Inputable, Activable {
  import raylib: Texture2D, Rectangle, Vector2, UnloadTexture;

  Texture2D objTexture;
  Motion motion;
  Rectangle src;
  Rectangle dest;
  Rectangle rectCollision; // this rectangle covers whole object
  Vector2 origin;

  ObjectName objectName;
  bool active=true;
  bool isColliding=false;

  this() {}

  ~this() {
    UnloadTexture(this.objTexture);
  }

  bool isActive() {
    return this.active;
  }

  void processInput() {}
  void update() {}
  void render() {}

} // end of class
