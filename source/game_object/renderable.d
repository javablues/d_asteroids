module renderable;

interface Renderable {
  void render();
}
