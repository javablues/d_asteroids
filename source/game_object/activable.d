module activable;

interface Activable {
  bool isActive();
}
