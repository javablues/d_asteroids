module asteroid_03;

import std.math;
import raylib;
import game_object;
import game_const;
import motion;

class Asteroid03 {
  const float scale=0.25f;
  const int asteroidWidth=500;
  const int asteroidHeight=360;

  Texture2D asteroidTexture;
  Rectangle src={0, 0, asteroidWidth, asteroidHeight};
  Rectangle dest={1000, 200, asteroidWidth*scale, asteroidHeight*scale};
  Vector2 origin={asteroidWidth*scale/2, asteroidHeight*scale/2};
  Motion motion={dx:1000, dy:200, rotation:0, maxSpeed:0, acceleration:4f};

  this() {
    this.asteroidTexture=LoadTexture("resources/asteroid/asteroid_03.png");
  }

  ~this() {
    UnloadTexture(this.asteroidTexture);
  }

  bool isActive() {
    return true;
  }

  void processInput() {
    this.motion.rotation+=0.4f;
    this.motion.acceleration=0.4f;
  }

  void update() {
    this.motion.dx += cos(this.motion.rotation*GameConst.radian) * this.motion.acceleration;
    this.motion.dy += sin(this.motion.rotation*GameConst.radian) * this.motion.acceleration;
    this.dest.x=this.motion.dx+0.01f;
    this.dest.y=this.motion.dy+0.01f;
  }

  void render() {
    DrawTextureTiled(asteroidTexture, this.src, this.dest, this.origin, this.motion.rotation+90, this.scale, Colors.WHITE);
  }

} // end of class
