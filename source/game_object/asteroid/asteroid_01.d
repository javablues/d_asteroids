module asteroid_01;

import std.math;
import raylib;
import game_object;
import motion;

class Asteroid01 : GameObject {
  import game_const;
  import object_name;

  const float scaledObjectWidth;
  const float scaledObjectHeight;

  this(int x, int y) {
    objectName=ObjectName.asteroid;
    objTexture=LoadTexture("resources/asteroid/asteroid_01.png");
    SetTextureFilter(objTexture, 1);
    scaledObjectWidth=objTexture.width*GameConst.scale;
    scaledObjectHeight=objTexture.height*GameConst.scale;

    src=Rectangle(0, 0, objTexture.width, objTexture.height);
    dest=Rectangle(0, 0, scaledObjectWidth, scaledObjectHeight);
    origin=Vector2(scaledObjectWidth/2, scaledObjectHeight/2);
    motion=Motion(x, y, 0, 0, 4f);

    rectCollision=Rectangle(0, 0, scaledObjectWidth, scaledObjectHeight);
  }

  override bool isActive() {
    return true;
  }

  override void processInput() {
    this.motion.rotation+=1f;
    this.motion.acceleration=0.2f;
  }

  override void update() {
    this.motion.dx+=cos(this.motion.rotation*GameConst.radian) * this.motion.acceleration;
    this.motion.dy+=sin(this.motion.rotation*GameConst.radian) * this.motion.acceleration;
    this.dest.x=this.motion.dx+0.01f;
    this.dest.y=this.motion.dy+0.01f;
    this.rectCollision.x=this.dest.x-scaledObjectWidth/2;
    this.rectCollision.y=this.dest.y-scaledObjectHeight/2;
  }

  override void render() {
    DrawTexturePro(this.objTexture, this.src, this.dest, this.origin, this.motion.rotation+90, Colors.WHITE);
    debug(rects) {
      DrawRectangleLinesEx(this.dest, 2, Colors.RED);
      DrawRectangleRounded(this.rectCollision, 1, 1, Colors.BLUE);
    }
  }

} // end of class
