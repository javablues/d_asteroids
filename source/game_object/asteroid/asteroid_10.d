module asteroid_10;

import std.math;
import raylib;
import game_object;
import game_const;
import motion;

class Asteroid10 {
  const float scale=0.25f;
  const int asteroidWidth=260;
  const int asteroidHeight=240;

  Texture2D asteroidTexture;
  Rectangle src={0, 0, asteroidWidth, asteroidHeight};
  Rectangle dest={700, 1200, asteroidWidth*scale, asteroidHeight*scale};
  Vector2 origin={asteroidWidth*scale/2, asteroidHeight*scale/2};
  Motion motion={dx:700, dy:1000, rotation:0, maxSpeed:0, acceleration:4f};

  this() {
    this.asteroidTexture=LoadTexture("resources/asteroid/asteroid_10.png");
  }

  ~this() {
    UnloadTexture(this.asteroidTexture);
  }

  bool isActive() {
    return true;
  }

  void processInput() {
    this.motion.rotation+=1.4f;
    this.motion.acceleration=0.2f;
  }

  void update() {
    this.motion.dx += cos(this.motion.rotation*GameConst.radian) * this.motion.acceleration;
    this.motion.dy += sin(this.motion.rotation*GameConst.radian) * this.motion.acceleration;
    this.dest.x=this.motion.dx+0.01f;
    this.dest.y=this.motion.dy+0.01f;
  }

  void render() {
    DrawTextureTiled(asteroidTexture, this.src, this.dest, this.origin, this.motion.rotation+90, this.scale, Colors.WHITE);
  }

} // end of class
