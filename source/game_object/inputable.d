module inputable;

interface Inputable {
  void processInput();
}
