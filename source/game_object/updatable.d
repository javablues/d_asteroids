module updatable;

interface Updatable {
  void update();
}
