module game_const;

struct GameConst {
  immutable static float radian=0.01745; // PI divided by 180
  immutable static float scale=0.25f;
  immutable static int fps=60;
  immutable static int screenWidth=1920;
  immutable static int screenHeight=1080;
}
