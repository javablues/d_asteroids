module gfx_helper;

struct GfxHelper {
  const int margin=20;

  bool isOffScreen(float x, float y) {
    import game_const;
    return (x<0-margin||y<0-margin) || (x>GameConst.screenWidth+margin||y>GameConst.screenHeight+margin);
  }

  import raylib: Vector2;
  Vector2 getPositionOnOppositeEdge(float x, float y) {
    import game_const;

    Vector2 position={x, y};
    if(x<0) position.x=GameConst.screenWidth+margin;
    if(x>GameConst.screenWidth) position.x=0-margin;
    if(y<0) position.y=GameConst.screenHeight+margin;
    if(y>GameConst.screenHeight) position.y=0-margin;

    return position;
  }

} // end of class
