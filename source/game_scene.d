module game_scene;

struct GameScene {
  import std.typecons: Tuple;
  import raylib;
  import game_object;
  import object_name;

  static GameObject[] gameObjects;

  alias ObjectPair=Tuple!(ObjectName, "obj1", ObjectName, "obj2");
  static bool[ObjectPair] collisionMatrix;

  static this() {
    // collision should be checked only for pairs that return 'true'
    collisionMatrix=[
      ObjectPair(ObjectName.spaceship, ObjectName.spaceship): false,
      ObjectPair(ObjectName.spaceship, ObjectName.shield): false,
      ObjectPair(ObjectName.spaceship, ObjectName.asteroid): true,
      ObjectPair(ObjectName.spaceship, ObjectName.bullet): false,

      ObjectPair(ObjectName.shield, ObjectName.spaceship): false,
      ObjectPair(ObjectName.shield, ObjectName.shield): false,
      ObjectPair(ObjectName.shield, ObjectName.asteroid): false,
      ObjectPair(ObjectName.shield, ObjectName.bullet): false,

      ObjectPair(ObjectName.asteroid, ObjectName.spaceship): true,
      ObjectPair(ObjectName.asteroid, ObjectName.shield): false,
      ObjectPair(ObjectName.asteroid, ObjectName.asteroid): false,
      ObjectPair(ObjectName.asteroid, ObjectName.bullet): true,

      ObjectPair(ObjectName.bullet, ObjectName.spaceship): false,
      ObjectPair(ObjectName.bullet, ObjectName.shield): false,
      ObjectPair(ObjectName.bullet, ObjectName.asteroid): true,
      ObjectPair(ObjectName.bullet, ObjectName.bullet): false
    ];
    collisionMatrix.rehash;
  }

  static void checkCollision(GameObject gameObject) {
    foreach(otherGameObject; GameScene.gameObjects) {
      if(collisionMatrix[ObjectPair(gameObject.objectName, otherGameObject.objectName)]) {
        if(CheckCollisionRecs(gameObject.rectCollision, otherGameObject.rectCollision)) {
          gameObject.isColliding=true;
          otherGameObject.isColliding=true;
          debug(rects) {
            import std.string;
            import std.conv;
            TraceLog(TraceLogLevel.LOG_DEBUG, ("Collision detected: " ~ to!string(ObjectPair(gameObject.objectName, otherGameObject.objectName))).toStringz);
          }
        }
      }
    }
  }

  static long gameObjectQty() {
    return gameObjects.length;
  }

  static string gameObjectQtyToStr() {
    import std.conv;
    import std.string;
    return to!string(gameObjectQty());
  }
}
