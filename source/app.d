module app;

void main() {
  import raylib;
  import game;
  import game_state;

  auto asteroids=new Game("Asteroids in D programming language with raylib");
  while(asteroids.state==GameState.play) {
    if(IsKeyPressed(KeyboardKey.KEY_F11)) ToggleFullscreen();
    if(WindowShouldClose()) {
      asteroids.state=GameState.stop;
    }
    asteroids.process();
  }

}
