module game;

class Game {
  import std.stdio;
  import std.string: toStringz;
  import std.random: uniform;
  import raylib;
  import game_scene;
  import game_state;
  import game_const;

  import cx16_spaceship;
  import asteroid_01;

  GameState state;
  Texture2D backgroundTexture;

  this(string title) {
    InitWindow(GameConst.screenWidth, GameConst.screenHeight, title.toStringz);
    SetTargetFPS(GameConst.fps);
    SetTraceLogLevel(TraceLogLevel.LOG_WARNING);
    HideCursor();
    debug(rects) {
      SetTraceLogLevel(TraceLogLevel.LOG_DEBUG);
    }

    this.state=GameState.play;
    this.backgroundTexture=LoadTexture("resources/background/background_01_static.png");

    GameScene.gameObjects~=new CX16Spaceship();

    int x=uniform(0, GameConst.screenWidth);
    int y=uniform(0, GameConst.screenHeight);
    GameScene.gameObjects~=new Asteroid01(x, y);
  }

  ~this() {
    UnloadTexture(backgroundTexture);
    CloseWindow();
  }

  void process() {
    BeginDrawing();
      ClearBackground(Colors.BLACK);
      DrawTexture(backgroundTexture, 0, 0, Colors.WHITE);
      DrawFPS(20, 20);
      DrawText(GameScene.gameObjectQtyToStr().toStringz, 120, 20, 20, Colors.BLUE);
      DrawText("d-asteroids", 20, 60, 20, Colors.RED);

      foreach(gameObject; GameScene.gameObjects) {
        GameScene.checkCollision(gameObject);

        gameObject.processInput();
        gameObject.update();
        gameObject.render();
      }
    EndDrawing();

    // remove from the scene all game objects that are not active
    import std.algorithm: remove;
    GameScene.gameObjects=GameScene.gameObjects.remove!(object=>!object.isActive());
  }

} // end of class
