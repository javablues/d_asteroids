module motion;

struct Motion {
  float dx;
  float dy;
  float rotation;
  float maxSpeed;
  float acceleration;
}
